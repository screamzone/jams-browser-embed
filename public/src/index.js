import { html, render, useEffect, useState } from "/lib/preact-with-htm.js";

const App = (props) => {
  const [jams, setJams] = useState([]);

  useEffect(() => {
    fetch("https://api.screamzone.org/v1/announced-jams").then(async (data) => {
      return setJams(await data.json());
    });
  }, []);

  const jamItems = jams.map((jam) => {
    const { title, date, imageUrl, jamUrl } = jam;

    const d = new Date(date);
    const month = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ][d.getMonth()];
    const year = d.getFullYear();

    return html`<a class="jam" href="${jamUrl}" target="_blank">
      <img class="image" alt="${title} Logo" src="${imageUrl}" />
      <div class="title">${title}</div>
      <div>${month} ${year}</div>
    </a>`;
  });

  return html`<div id="container">${jamItems}</div>`;
};

render(html`<${App} />`, document.body);
